#include <iostream>

int main()
{
	const int N = 10;
	int i = 0;
	int array[N][N];
	for (i; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
				array[i][j] = i + j;
				std::cout << array[i][j] << " ";
		}
		std::cout << "\n";
	}

	int day;
	int sum = 0;
	std::cout << "What day is it today? (choose from 1 to 31) ";
	std::cin >> day;
	i = day % N;
	
	for (int k = 0; k < N; k++)
	{
		sum = sum + array[i][k];
	}
	std::cout << sum << " ";
}